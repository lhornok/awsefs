##########################################
# VPC / SUBNETS                          #
##########################################

resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "main"
  }
}


# Subnet EC2
resource "aws_subnet" "private_subnet_a" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.101.0/24"

  tags = {
    Name = "private_subnet_a"
  }
}


# Subnet EFS
resource "aws_subnet" "private_subnet_efs" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.108.0/24"

  tags = {
    Name = "private_subnet_efs"
  }
}

