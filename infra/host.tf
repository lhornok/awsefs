#################################################################################
# EC2 INSTANCE                                                                  #
# Ubuntu Server 18.04 LTS (HVM), SSD Volume Type                                #
################################################################################

resource "aws_instance" "kubemaster" {
  ami                         = "ami-0aef57767f5404a3c"
  instance_type               = "t2.micro"
  key_name                    = "deployer-key"
  subnet_id                   = aws_subnet.private_subnet_a.id
  vpc_security_group_ids      = [aws_security_group.sg_ssh.id]
  associate_public_ip_address = true
  source_dest_check           = false
}
