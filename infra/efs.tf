# efs.tf
resource "aws_efs_file_system" "hooli-efs" {
  creation_token = "hooli-efs"
  performance_mode = "generalPurpose"
  throughput_mode = "bursting"
  encrypted = "true"
  tags = {
    Name = "HooliEfs"
  }
}

resource "aws_efs_mount_target" "hooli-efs-mount" {
  ip_address = "10.0.108.101"
  file_system_id  = "${aws_efs_file_system.hooli-efs.id}"
  subnet_id = "${aws_subnet.private_subnet_efs.id}"
  security_groups = ["${aws_security_group.ingress-efs-hooli.id}"]
}

resource "aws_security_group" "ingress-efs-hooli" {
  name = "ingress-efs-test-sg"
  vpc_id = "${aws_vpc.main.id}"

  # NFS
  ingress {
    cidr_blocks = [aws_subnet.private_subnet_a.cidr_block]
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
  }

  # Terraform removes the default rule
  egress {
    cidr_blocks = [aws_subnet.private_subnet_a.cidr_block]
    from_port = 0
    to_port = 0
    protocol = "-1"
  }
}